def insertion_sort(ins) :
    for i in range(1,len(ins)) :
        value = ins[i]
        x = i - 1
        while x >= 0 :
            if value < ins[x] :
                ins[x+1] = ins[x]
                ins[x] = value
                x = x - 1
            else :
                break
list_numbers = [int(x) for x in input("Enter your number: ").split()]
insertion_sort(list_numbers)
print(list_numbers)